const {DocumentStore} = require("ravendb");
const Materia = require("./Materia.js").modules
const store = new DocumentStore([' http://127.0.0.1:8080'],"Universidad").initialize();
const sesion = store.openSession()

const inicializarMaterias = async (sesion) => {
    
    const materia = new Materia(
    "Base de datos II",
     [{"com" : 1},{"com" : 2}],
     ["Pablo Sabatino", "Guillermo Salvatierra"]
    )
    const materia2 = new Materia(
        "Relaciones Publicas",
         [{"com" : 1}],
         ["Agustin Pacheco"]
    )
    const materia3 = new Materia(
        "PSEC",
         [{"com" : 1}],
         ["Agustin Canapino"]
    )
    await sesion.store(materia);
    await sesion.store(materia2);
    await sesion.store(materia3);
    await sesion.saveChanges();                       
}

const buscarMateria = async(sesion) => {
     const materia = await sesion
    .query({ collection: "Materias" })
    .whereEquals("nombreMateria", "Base de datos II")
    .all();
    materia.forEach(materia => {
        console.log(`Materia: ${materia.nombreMateria}, Comisiones: ${materia.comisiones}, Profesores : ${materia.profesores}`);
    });
}


const updateMateria = async(sesion) => {
    const materia = await sesion
    .query({ collection: "Materias" })
    .whereEquals("nombreMateria", "PSEC")
    .all();


    let materiaCargada = await sesion.load(materia[0].id);

    console.log(materiaCargada)

    materiaCargada.profesores = ["Alvaro Pernia"];

    sesion.saveChanges();
}


const deleteMateria = async(sesion) => {
    const materia = await sesion
    .query({ collection: "Materias" })
    .whereEquals("nombreMateria", "Relaciones Publicas")
    .all();

    materia.forEach(async (materia) => {

        await sesion.delete(materia.id);
        await sesion.saveChanges();
    })

    const materiasEliminada = await sesion
    .query({ collection: "Materias" })
    .whereEquals("nombreMateria", "Relaciones Publicas")
    .all();

    materiasEliminada.forEach(materia => {
        console.log(materia)
    })
}



 async function main (){
    //await inicializarMaterias(sesion);
   // await buscarMateria(sesion);
   // await updateMateria(sesion);
    await deleteMateria(sesion);
}

main();